# Készítette: Csóti Zoltán és Havriló Balázs
#encoding: utf-8

import time
from Client import SocketClient
import json
import numpy as np
from os.path import exists
import random
import math


# stratégiánk implementációja távoli eléréshez.
class PythronicsStrategy:
    
    # függvények stratégiákhoz
    def __init__(self):
        # Dinamikus viselkedéshez szükséges változók definíciója
        self.oldpos = None
        self.oldcounter = 0

    # Egyéb függvények...
    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action
    
    # Fitnesz függvényhez negálás
    def neg(self,x):
        if x==0:
            y=1
        else:
            y=0
        return y

    # mi stratégiáink
    def RandomStrategy(self, fulljson, sendData):
        jsonData = fulljson["payload"]
        if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                                  
            index=0
            possibleactions=[]
            for field in jsonData["vision"]:
                nextfield=tuple(field["relative_coord"])
                
                if nextfield == (1,1) or nextfield == (0,1) or nextfield == (1,0) or nextfield == (-1,0) or nextfield == (-1,1) or nextfield == (0,-1) or nextfield == (1,-1) or nextfield == (-1,-1):
                     if field["value"] != 9: # itt nincs fal, kigyűjtjük a jó lépéseket
                         possibleactions.append(nextfield)                 
                         index=index+1
               
            actionindex = random.randint(0, index-1) # véletlen generálunk egyet a jó lépések közül      
                                     
            r = possibleactions[actionindex]
            # átkonvertálás
            actdict = {0: "0", 1: "+", -1: "-"}
            actstring = ""
            for act in r:
                    actstring += actdict[act]
        return actstring

    def HunterStrategy(self, fulljson, sendData):
            jsonData = fulljson["payload"]
            if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                if self.oldpos is not None:
                    if tuple(self.oldpos) == tuple(jsonData["pos"]):
                        self.oldcounter += 1
                    else:
                        self.oldcounter = 0
                if jsonData["active"]:
                    self.oldpos = jsonData["pos"].copy()

                vals = []
                for field in jsonData["vision"]:
                    if field["player"] is not None:
                        if tuple(field["relative_coord"]) == (0, 0):
                            if 0 < field["value"] <= 3:
                                vals.append(field["value"])
                            elif field["value"] == 9:
                                vals.append(-1)
                            else:
                                vals.append(0)
                        elif field["player"]["size"] * 1.1 < jsonData["size"]:
                            vals.append(field["player"]["size"])
                        else:
                            vals.append(-1)
                    else:
                        if 0 < field["value"] <= 3:
                            vals.append(field["value"])
                        elif field["value"] == 9:
                            vals.append(-1)
                        else:
                            vals.append(0)

                values = np.array(vals)

                if np.max(values) <= 0 or self.oldcounter >= 3:
                    actstring = self.getRandomAction()
                    self.oldcounter = 0
                else:
                    idx = np.argmax(values)
                    actstring = ""
                    for i in range(2):
                        if jsonData["vision"][idx]["relative_coord"][i] == 0:
                            actstring += "0"
                        elif jsonData["vision"][idx]["relative_coord"][i] > 0:
                            actstring += "+"
                        elif jsonData["vision"][idx]["relative_coord"][i] < 0:
                            actstring += "-"
            return actstring
        
        
    def NaiveStrategy(self, fulljson, sendData):
        jsonData = fulljson["payload"]
        if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
            if self.oldpos is not None:
                if tuple(self.oldpos) == tuple(jsonData["pos"]):
                    self.oldcounter += 1

            if jsonData["active"]:
                self.oldpos = jsonData["pos"].copy()
            
            values = np.array([field["value"] for field in jsonData["vision"]])
            values[values > 3] = 0
            values[values < 0] = 0
            if np.max(values) == 0 or self.oldcounter >= 3:
                actstring = self.getRandomAction()
                self.oldcounter = 0   
                
            else:
                idx = np.argmax(values)
                actstring = ""
                for i in range(2):
                    if jsonData["vision"][idx]["relative_coord"][i] == 0:
                        actstring += "0"
                    elif jsonData["vision"][idx]["relative_coord"][i] > 0:
                        actstring += "+"
                    elif jsonData["vision"][idx]["relative_coord"][i] < 0:
                        actstring += "-"
            
        return actstring

    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):
        
        # Bemenetek előállítása
        gametype = fulljson.get("type")
        if gametype == "gameData":
            payload = fulljson.get("payload")
            siz = payload.get("size")
            #print("size:",siz)
            tick = payload.get("tick")
            #print("tick:",tick)            
            vision = payload.get("vision")
            #print("vision:",vision)
            abspos = payload.get("pos")
            #print("abspos:",abspos)
          
            visionarray=np.zeros((len(vision),4)) # 81x4-es, RELX_RELY_ENEMYSIZE_FIELDVALUE
            enemyhistory=np.zeros(3) # utolsó 3 alkalommal hány ellenfél volt
            ourhistory=np.zeros(3) # utolsó 3 alkalommal léptünk-e
            enemyfound=0 # talált-e ellenfelet ebben a tick-ben
            absx=abspos[0] # mi pozíciónk
            absy=abspos[1]
            absx=str(absx)
            absy=str(absy)
            newpos=int(absx+absy)
            oldpos=newpos # első futásnál a pozíciónk
            
            for i in range(len(vision)):       # összes látómező lekérdezése
                    player=vision[i].get("player")
                    relx=vision[i].get("relative_coord")[0]
                    rely=vision[i].get("relative_coord")[1]
                    fieldvalue=vision[i].get("value")
                    
                    
                    if player is None: # ha nincs senki a mezőn   
                        #enemyname=player # névre nincs szükség - None-t ad
                        enemysiz=0
                        
                                                
                    else: # ha van játékos (magunkat beleértve) a mezőn
                       
                                             
                       if vision[i].get("player").get("name")=="RemotePlayer": # a saját mezőm
                           enemysiz=0
                           
                       else:
                           #enemyname=vision[i].get("player").get("name") # ha ellenfél van a mezőn
                           enemysiz=vision[i].get("player").get("size")
                           if siz>enemysiz*1.1:
                               enemyfound=enemyfound+1
                                                 
                    visionarray[i][0]=relx
                    visionarray[i][1]=rely
                    visionarray[i][2]=enemysiz
                    visionarray[i][3]=fieldvalue
             
            # Hány szomszédos mező tartalamz falat?
            wallnumber=0
            for i in range(np.shape(visionarray)[0]):
                    xyarray=[visionarray[i][0],visionarray[i][1]]
                    if xyarray == [1,1] or xyarray == [0,1] or xyarray == [1,0] or xyarray == [-1,0] or xyarray == [-1,1] or xyarray == [0,-1] or xyarray == [1,-1] or xyarray == [-1,-1]:
                        
                        if visionarray[i][3]==9:
                            wallnumber=wallnumber+1
                            
            # Hány mező tartalmaz kaját a látótávban?
            foodnumber=0
            for i in range(np.shape(visionarray)[0]):
                if visionarray[i][3]==1 or visionarray[i][3]==2 or visionarray[i][3]==3:
                    foodnumber=foodnumber+1
             
                    
            # fájlkezelés - kiolvasni enemyhistoryt és frissíteni
            # .txt fájlban utolsó 3 körben mennyi ellenfelet láttunk, utolsó 3 körben hányszor változtattunk pozíciót, korábbi X absz pozíció, korábbi Y absz pozíció
            
            file_exists = exists(".\history.txt") and tick!=0 # megnézi, hogy létezik-e a fájl, első tickben ne olvassa el az előző meccs eredményét
            if file_exists:
                historyfile = open(r".\history.txt", "r") # megnyitni olvasásra, enemyhistory kiolvasása
                data=historyfile.read()               
                oldpos=""
                
                for j in range(len(data)):
                    if j<3:
                        enemyhistory[j]=int(data[j])
                    elif 3<=j and j<6:
                        ourhistory[j-3]=int(data[j])
                    elif 6<=j:
                        oldpos=oldpos+data[j]
                oldpos=int(oldpos)        
                historyfile.close()
            
            # ourhistory frissítése
            if abs(newpos-oldpos) < 0.1:
                posdiff=0
            else:
                posdiff=1
                
            ourhistory[2]=ourhistory[1]
            ourhistory[1]=ourhistory[0]
            ourhistory[0]=posdiff
            
            # enemyhistory frissítése
            enemyhistory[2]=enemyhistory[1]
            enemyhistory[1]=enemyhistory[0]
            enemyhistory[0]=enemyfound
                        
            
            # legelső tick-nél létrehozza a fájlt
            # de minden tick végén felül kell írnia az aktuális értékekkel
            historyfile = open(r".\history.txt", "w")
            for j in range(3):
                historyfile.write(str(int(enemyhistory[j])))
            for j in range(3):
                historyfile.write(str(int(ourhistory[j]))) 
            historyfile.write(str(int(absx))) 
            historyfile.write(str(int(absy)))
            historyfile.close()
                
            
        # A bemenetek összegyűjtése után a feldolgozás következik.
        """
        :param fulljson: A játékmestertől érkező JSON dict-be konvertálva.
        Két kötelező kulccsal: 'type' (leaderBoard, readyToStart, started, gameData, serverClose) és 'payload' (az üzenet adatrésze).
        'leaderBoard' type a játék végét jelzi, a payload tartalma {'ticks': a játék hossza tickekben, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során},...]}
        'readyToStart' type esetén a szerver az indító üzenetre vár esetén, a payload üres (None)
        'started' type esetén a játék elindul, tickLength-enként kiküldés és akciófogadás várható payload {'tickLength': egy tick hossza }
        'gameData' type esetén az üzenet a játékos által elérhető információkat küldi, a payload:
                                    {"pos": abszolút pozíció a térképen, "tick": az aktuális tick sorszáma, "active": a saját életünk állapota,
                                    "size": saját méret,
                                    "leaderBoard": {'ticks': a játék hossza tickekben eddig, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során eddig},...]},
                                    "vision": [{"relative_coord": az adott megfigyelt mező relatív koordinátája,
                                                                    "value": az adott megfigyelt mező értéke (0-3,9),
                                                                    "player": None, ha nincs aktív játékos, vagy
                                                                            {name: a mezőn álló játékos neve, size: a mezőn álló játékos mérete}},...] }
        'serverClose' type esetén a játékmester szabályos, vagy hiba okozta bezáródásáról értesülünk, a payload üres (None)
        :param sendData: A kliens adatküldő függvénye, JSON formátumú str bemenetet vár, melyet a játékmester felé továbbít.
        Az elküldött adat struktúrája {"command": Parancs típusa, "name": A küldő azonosítója, "payload": az üzenet adatrésze}
        Elérhető parancsok:
        'SetName' A kliens felregisztrálja a saját nevét a szervernek, enélkül a nevünkhöz tartozó üzenetek nem térnek vissza.
                 Tiltott nevek: a configban megadott játékmester név és az 'all'.
        'SetAction' Ebben az esetben a payload az akció string, amely két karaktert tartalmaz az X és az Y koordináták (matematikai mátrix indexelés) menti elmozdulásra.
                a karakterek értékei '0': helybenmaradás az adott tengely mentén, '+' pozitív irányú lépés, '-' negatív irányú lépés lehet. Amennyiben egy tick ideje alatt
                nem külünk értéket az alapértelmezett '00' kerül végrehajtásra.
        'GameControl' üzeneteket csak a Config.py-ban megadott játékmester névvel lehet küldeni, ezek a játékmenetet befolyásoló üzenetek.
                A payload az üzenet típusát (type), valamint az ahhoz tartozó 'data' adatokat kell, hogy tartalmazza.
                    'start' type elindítja a játékot egy "readyToStart" üzenetet küldött játék esetén, 'data' mezője üres (None)
                    'reset' type egy játék után várakozó 'leaderBoard'-ot küldött játékot állít alaphelyzetbe. A 'data' mező
                            {'mapPath':None, vagy elérési útvonal, 'updateMapPath': None, vagy elérési útvonal} formátumú, ahol None
                            esetén az előző pálya és növekedési map kerül megtartásra, míg elérési útvonal megadása esetén új pálya kerül betöltésre annak megfelelően.
                    'interrupt' type esetén a 'data' mező üres (None), ez megszakítja a szerver futását és szabályosan leállítja azt.
        :return:
        """

        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")
            for score in fulljson["payload"]["players"]:
                print(score["name"],score["active"], score["maxSize"])

            time.sleep(50)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            time.sleep(5)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])
        

        # Akció előállítása bemenetek alapján
        elif fulljson["type"] == "gameData":
            
                Npop=10 # popuációméret            
                # Ha nem az első körben vagyunk, kiolvassuk aaz előző populációt            
                file_exists = exists(".\population.txt") and tick!=0 # megnézi, hogy létezik-e a fájl, első tickben ne olvassa el az előző meccs eredményét
                if file_exists:
                    popfile = open(r".\population.txt", "r") # megnyitni olvasásra, enemyhistory kiolvasása
                    spec=np.zeros((Npop,2))
                    for i in range(Npop): # végigmegyünk a fájlon
                        data = popfile.readline()
                        
                        X=""
                        Y=""
                        commafound=0
                        for j in range(len(data)): # beolvasunk 1 sort
                            if commafound==1:
                                Y=Y+data[j]
                            if data[j]==",":
                                commafound=1
                            if commafound==0:
                                X=X+data[j]
                            
                        X=float(X)
                        Y=float(Y)
                        spec[i][0]=X
                        spec[i][1]=Y
                                           
                    popfile.close()
                    
                else:    
                    # A legelső körben le kell gyártani random egyedeket
                    spec=[] # teljes populáció mátrixa, sorai az egyedek, melyek rendelkeznek egy szándékosság és egy aggresszió értékkel
                    
                    for i in range(Npop):
                        spec.append(np.random.randint(0, 101, 2))  
                
                # Fitnesz értékek számolása
                Fszum=np.zeros(Npop) # egyedek fitnesz értékeit tároló vektor
                for i in range(Npop):
                    # hunter vs naív
                    Fhunter=spec[i][1]*enemyhistory[0]*1.4+spec[i][1]*enemyhistory[1]*1.2+spec[i][1]*enemyhistory[2]
                    Fnaive=self.neg(enemyhistory[0])*(100-spec[i][1])*1.4+self.neg(enemyhistory[1])*(100-spec[i][1])*1.2+self.neg(enemyhistory[2])*(100-spec[i][1])
                    Fagg=Fhunter+Fnaive
                    
                    # szándékos vs véletlen
                    # kajától függő rész
                    Ffoodint=foodnumber*spec[i][0]/6
                    Ffoodrand=(100-spec[i][0])*5/(foodnumber+1)                
                    Ffood=Ffoodint+Ffoodrand
                    # falaktól függő rész
                    Fwallsint=spec[i][0]*(7-wallnumber)*0.5
                    Fwallsrand=wallnumber*(100-spec[i][0])*0.5
                    Fwalls=Fwallsint+Fwallsrand
                    # korábbi mozdulatlanságtól függő rész
                    Fstuckint=spec[i][0]*ourhistory[0]*2+spec[i][0]*ourhistory[1]*1.5+spec[i][0]*ourhistory[2]
                    Fstuckrand=self.neg(ourhistory[0])*(100-spec[i][0])*2+self.neg(ourhistory[1])*(100-spec[i][0])*1.5+self.neg(ourhistory[2])*(100-spec[i][0])
                    Fstuck=Fstuckint+Fstuckrand
                    # 3 rész összegzése
                    Fint=Fstuck+Fwalls+Ffood
                    
                    # teljes fitnesz
                    Fszum[i]=Fagg+Fint
                                
                # Rulettkerék szelekció
                # terület feolsztása
                area=np.sum(Fszum)
                separators=np.zeros(Npop+1)
                
                for i in range(Npop):
                    for j in range(i):
                        separators[i]=separators[i]+Fszum[j]
                
                separators[-1]=area
                
                # gurítás
                chosen=np.zeros(Npop) # kiválasztott egyedek sorszámai
                
                for j in range(Npop):
                    rulett = random.randint(0, math.floor(area)-1)
                    
                    for i in range(Npop):
                        if separators[i]<=rulett and rulett<separators[i+1]:
                            chosen[j]=i 
                    
                # Keresztezés
                # valós számokra keresztezés
                offspring=np.zeros((Npop,2))
                
                for i in range(0,Npop,2):
                    alfa=random.random() # keresztezés paramétere 0 és 1 közti szám
                   
                    offspring[i][0]=alfa*spec[int(chosen[i])][0]+(1-alfa)*spec[int(chosen[i+1])][0] # új szándékosság
                    offspring[i][1]=alfa*spec[int(chosen[i])][1]+(1-alfa)*spec[int(chosen[i+1])][1] # új aggresszió
                    offspring[i+1][0]=(1-alfa)*spec[int(chosen[i])][0]+alfa*spec[int(chosen[i+1])][0] # új szándékosság
                    offspring[i+1][1]=(1-alfa)*spec[int(chosen[i])][1]+alfa*spec[int(chosen[i+1])][1] # új aggresszió
                                
                # Mutáció
                for i in range(Npop):
                    alfa=random.random()
                    if alfa < 0.1: # 10% valószínűséggel mutál egy tulajdonságot
                        beta=(random.random()-0.5)*10 
                        offspring[i][0]=offspring[i][0]+beta # tulajdonság megváltoztatása -5 ... +5 értékkel
                        # ne tudjunk kilépni a 0-100 skálahatárokból
                        if offspring[i][0]>100:
                            offspring[i][0]=100
                        if offspring[i][0]<0:
                            offspring[i][0]=0
                            
                    alfa=random.random()
                    if alfa < 0.1:
                        beta=(random.random()-0.5)*10
                        offspring[i][1]=offspring[i][1]+beta
                        # ne tudjunk kilépni a 0-100 skálahatárokból
                        if offspring[i][1]>100:
                            offspring[i][1]=100
                        if offspring[i][1]<0:
                            offspring[i][1]=0
               
                # Fitnesz értékek számolása az algoritmus végén, meg kell találni a legjobb egyedet
                Fszum=np.zeros(Npop) 
                for i in range(Npop):
                    # hunter vs naív
                    Fhunter=offspring[i][1]*enemyhistory[0]*1.4+offspring[i][1]*enemyhistory[1]*1.2+offspring[i][1]*enemyhistory[2]
                    Fnaive=self.neg(enemyhistory[0])*(100-offspring[i][1])*1.4+self.neg(enemyhistory[1])*(100-offspring[i][1])*1.2+self.neg(enemyhistory[2])*(100-offspring[i][1])
                    Fagg=Fhunter+Fnaive
                    
                    # szándékos vs véletlen
                    # kajától függő rész
                    Ffoodint=foodnumber*offspring[i][0]/6
                    Ffoodrand=(100-offspring[i][0])*5/(foodnumber+1)                
                    Ffood=Ffoodint+Ffoodrand
                    # falaktól függő rész
                    Fwallsint=offspring[i][0]*(7-wallnumber)*0.5
                    Fwallsrand=wallnumber*(100-offspring[i][0])*0.5
                    Fwalls=Fwallsint+Fwallsrand
                    # korábbi mozdulatlanságtól függő rész
                    Fstuckint=offspring[i][0]*ourhistory[0]*2+offspring[i][0]*ourhistory[1]*1.5+offspring[i][0]*ourhistory[2]
                    Fstuckrand=self.neg(ourhistory[0])*(100-offspring[i][0])*2+self.neg(ourhistory[1])*(100-offspring[i][0])*1.5+self.neg(ourhistory[2])*(100-offspring[i][0])
                    Fstuck=Fstuckint+Fstuckrand
                    # 3 rész összegzése
                    Fint=Fstuck+Fwalls+Ffood
                    
                    # teljes fitnesz
                    Fszum[i]=Fagg+Fint
                
                # Legjobb egyed megtalálása
                fit=0
                for i in range(Npop):
                    if Fszum[i]>fit:
                        best=offspring[i]
                        fit=Fszum[i]
                
                # Stratégia előállítása, melyik stratégiát válasszuk a következő 3 körre
                file_exists = exists(".\strategy.txt")  # megnézi, hogy létezik-e a fájl, első tickben ne olvassa el az előző meccs eredményét
                if file_exists and (tick%3==1 or tick%3==2):
                    stratfile = open(r".\strategy.txt", "r") # megnyitni olvasásra, enemyhistory kiolvasása
                    strat=int(stratfile.read())
                    stratfile.close()
                
                else:
                    decider=random.randint(0, 100)
                    if  best[0]>= decider: # szándékos vs véletlen
                        
                        decider=random.randint(0, 100) # aggresszív vs naív
                        if best[1]>= decider:
                             strat=0
                              
                        else:
                             strat=1
                                                 
                    else:
                        strat=2
                                               
                # Döntéshozatal
                if strat==0:                  
                    actstring=self.HunterStrategy(fulljson, sendData) # stratégia meghívása
                    
                if strat==1:                    
                    actstring=self.NaiveStrategy(fulljson, sendData) # stratégia meghívása 
                    
                if strat==2:                   
                    actstring=self.RandomStrategy(fulljson, sendData) # stratégia meghívása 
                    
                # Utód populáció elmentése a következő tick-re
                # legelső tick-nél létrehozza a fájlt
                # de minden tick végén felül kell írnia az aktuális értékekkel
                popfile = open(r".\population.txt", "w")
                for i in range(Npop):
                    popfile.write(str(offspring[i][0]))
                    popfile.write(",")
                    popfile.write(str(offspring[i][1]))
                    popfile.write("\n")
                popfile.close()
                
                # 0. tick-ben és minden 3. tick-ben fájlba írjuk, hogy a következő 3 tick-ben ugyanazt döntsük 
                if tick==0 or (tick % 3)==0:
                    stratfile=open(r".\strategy.txt", "w")
                    stratfile.write(str(strat))
                    stratfile.close()
                        
                # Akció JSON előállítása és elküldése                
                sendData(json.dumps({"command": "SetAction", "name": "RemotePlayer", "payload": actstring}))



if __name__=="__main__":
    # Példányosított stratégia objektum
    ourstrategy = PythronicsStrategy()
 
    

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    client = SocketClient("localhost", 42069, ourstrategy.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "RemotePlayer", "payload": None}))

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.